J = 128;
r = 1/2;
% r = 2;
eps = 0.01;

% Time info
T = 1;
N = 100;
dt = T/N;

% Space info
XInt = [0 1];
dx = diff(XInt) / J;
x = XInt(1):dx:(XInt(2)-dx);

q = @(j) j.^(-(2*r+1+eps));
a = diff(XInt);
eigenVecJ = @(j) sqrt(q(j)*2/a)*sin(pi*j*x/a);

eigenVec = arrayfun(eigenVecJ,1:J-1,'UniformOutput',false);
eigenVec = cell2mat(eigenVec');

% dW = QWienerProcess(eigenVec,N,dt);
brownMot = sqrt(dt)*randn(N,size(eigenVec,1));
dW = brownMot*eigenVec;
    
W = cumsum(dW);
surf([W W(:,1)])