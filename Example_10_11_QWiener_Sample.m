J = 100;
r = 1;
eps = 0.01;

% Time info
T = 1;
N = 100;
dt = T/N;

% Space info
XInt = [0 1];
dx = diff(XInt) / J;
x = XInt(1):dx:(XInt(2)-dx);

q = @(j) j.^(-(2*r+1+eps));
a = diff(XInt);
eigenVecJ = @(j) sqrt(q(2*abs(j)+1)/a)*exp(1i*2*pi*j*x/a);

eigenVec = arrayfun(eigenVecJ,(-J/2+1):(J/2),'UniformOutput',false);
eigenVec = cell2mat(eigenVec');

% dW = QWienerProcess(eigenVec,N,dt);
brownMot = zeros(N,size(eigenVec,1));
brownMot(:,J/2) = sqrt(dt)*randn(N,1);
brownMot(:,end) = sqrt(dt)*randn(N,1);
complexNormalDim = size(brownMot(brownMot==0));
brownMot(brownMot==0) = sqrt(dt/2)*(randn(complexNormalDim) + 1i*randn(complexNormalDim));
% brownMot = sqrt(dt)*randn(N,size(eigenVec,1));
dW = brownMot*eigenVec;


W = cumsum(dW);
surf(abs([W W(:,1)]))