J = 512;
alpha = 0.05;
% alpha = 0.5;
eps = 0.01;

% Time info
T = 1;
N = 100;
dt = T/N;

% Space info
XInt = [0 2*pi 0 2*pi];
dx = (XInt(1:2:end) - XInt(2:2:end)) / J;
x = XInt(1):dx(1):(XInt(2)-dx(1));
y = XInt(3):dx(2):(XInt(4)-dx(2));
[X,Y] = meshgrid(x,y);

q = @(j) exp(-alpha*(j1^2+j2^2));
a = XInt(2:2:end)-XInt(1:2:end);
eigenVecJ = @(j1,j2) sqrt(q(j1,j2)/prod(a))*exp(1i*2*pi*(j1*X/a+j2*Y/a));

eigenVec = arrayfun(eigenVecJ,(-J/2+1):(J/2),'UniformOutput',false);
eigenVec = cell2mat(eigenVec');

% dW = QWienerProcess(eigenVec,N,dt);
brownMot = zeros(N,size(eigenVec,1));
brownMot(:,J/2) = sqrt(dt)*randn(N,1);
brownMot(:,end) = sqrt(dt)*randn(N,1);
complexNormalDim = size(brownMot(brownMot==0));
brownMot(brownMot==0) = sqrt(dt/2)*(randn(complexNormalDim) + 1i*randn(complexNormalDim));
% brownMot = sqrt(dt)*randn(N,size(eigenVec,1));
dW = brownMot*eigenVec;


W = cumsum(dW);
surf(abs([W W(:,1)]))