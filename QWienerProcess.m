function [dW] = QWienerProcess(eigenVec,N,dt)
% Eigenvector matrix is already weighted by the coeffient, dimension (J x M)
% Aiming for output of dimension (N x M)
    brownMot = sqrt(dt)*randn(N,size(eigenVec,1));
    dW = brownMot*eigenVec;
end

